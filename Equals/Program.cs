﻿using System;
using System.Text;

namespace Equals
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = "test";
            string s2 = "test";
            string s3 = "test1".Substring(0, 4);
            object s4 = s3;
            //output, True True True
            Console.WriteLine("{0} {1} {2}", object.ReferenceEquals(s1, s2), s1 == s2, s1.Equals(s2));
            //output, False True True
            Console.WriteLine("{0} {1} {2}", object.ReferenceEquals(s1, s3), s1 == s3, s1.Equals(s3));
            //output, False False True
            Console.WriteLine("{0} {1} {2}", object.ReferenceEquals(s1, s4), s1 == s4, s1.Equals(s4));
            Console.WriteLine("------------------------------------------");
            //a and b are reference types containing strings (you would be right)
            //Equals is overridden in the string class to do an equivalence (value) comparison, and the values are equal. So a.Equals(b) is true (you would still be right).
            //However, a == b is an overload and on the object type it does an identity comparison, not a value comparison (you would still be right).
            //a and b are separate objects in memory so a == b is false (you would be wrong)
            object a = "Hello World";
            object b = "Hello World";
            //
            //output true
            Console.WriteLine(a.Equals(b));
            //output true
            Console.WriteLine(a == b);
            Console.WriteLine("------------------------------------------");
            object a1 = "Hello World";
            object b1 = new StringBuilder().Append("Hello").Append(" World").ToString();
            //output true
            Console.WriteLine(a1.Equals(b1));
            //output true
            Console.WriteLine(object.Equals(b1, a1));
            //output false
            Console.WriteLine(a1 == b1);
            Console.Read();
        }
    }
}
